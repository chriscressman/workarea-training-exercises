module Workarea
  module Pricing
    module Calculators
      class BigAndTallCalculator
        include Calculator

        def adjust
          order.items.each do |item|
            item_pricing_sku = pricing.records.detect { |r| r.id.to_s == item.sku }
            next unless item_pricing_sku.big_and_tall?

            surcharge_pricing = Collection.new('big-and-tall-surcharge')
            surcharge_price = surcharge_pricing
                                   .first
                                   .find_price(quantity: item.quantity)

            surcharge_unit = surcharge_price.sell

            if surcharge_unit > 0
              item.adjust_pricing(
                price: 'item',
                amount: surcharge_unit * item.quantity,
                quantity: item.quantity,
                calculator: self.class.name,
                description: 'Big & Tall',
                data: { 'tax_code' => surcharge_price.tax_code }
              )
            end
          end
        end
      end
    end
  end
end
