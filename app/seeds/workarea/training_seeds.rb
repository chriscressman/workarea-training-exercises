module Workarea
  class TrainingSeeds
    def perform
      puts 'Adding training data...'

      # Add some products with variants
      Catalog::Product.create!(
        name: 'Shirt',
        details: {
          'Fit' => ['Classic'],
          'Material' => ['Cotton', 'Polyester']
        },
        variants: [
          { sku: 'large-shirt', details: { 'Size' => ['L'] } },
          { sku: 'extra-large-shirt', details: { 'Size' => ['XL'] } },
          { sku: '2-extra-large-shirt', details: { 'Size' => ['XXL'] } },
          { sku: '3-extra-large-shirt', details: { 'Size' => ['XXXL'] } }
        ]
      )

      Catalog::Product.create!(
        name: 'Jacket',
        details: {
          'Fit' => ['Classic'],
          'Material' => ['Cotton', 'Polyester']
        },
        variants: [
          {
            sku: '46-regular-jacket',
            details: { 'Chest' => ['46'], 'Length' => ['Regular'] }
          },
          {
            sku: '48-regular-jacket',
            details: { 'Chest' => ['48'], 'Length' => ['Regular'] }
          },
          {
            sku: '48-long-jacket',
            details: { 'Chest' => ['48'], 'Length' => ['Long'] }
          },
          {
            sku: '50-regular-jacket',
            details: { 'Chest' => ['50'], 'Length' => ['Regular'] }
          }
        ]
      )

      Catalog::Product.create!(
        name: 'Hat',
        details: {
          'Size' => ['One Size Fits All'],
          'Material' => ['Cotton']
        },
        variants: [ { sku: 'hat' } ]
      )

      # Add a pricing sku for each product variant
      Pricing::Sku.create!(
        _id: 'large-shirt',
        tax_code: '001',
        prices: [
          { regular: 60 }
        ]
      )

      Pricing::Sku.create!(
        _id: 'extra-large-shirt',
        tax_code: '001',
        prices: [
          { regular: 60 }
        ]
      )

      Pricing::Sku.create!(
        _id: '2-extra-large-shirt',
        tax_code: '001',
        prices: [
          { regular: 60 }
        ]
      )

      Pricing::Sku.create!(
        _id: '3-extra-large-shirt',
        tax_code: '001',
        prices: [
          { regular: 60 }
        ]
      )

      Pricing::Sku.create!(
        _id: '46-regular-jacket',
        tax_code: '001',
        prices: [
          { regular: 120 }
        ]
      )

      Pricing::Sku.create!(
        _id: '48-regular-jacket',
        tax_code: '001',
        prices: [
          { regular: 120 }
        ]
      )

      Pricing::Sku.create!(
        _id: '48-long-jacket',
        tax_code: '001',
        prices: [
          { regular: 120 }
        ]
      )

      Pricing::Sku.create!(
        _id: '50-regular-jacket',
        tax_code: '001',
        prices: [
          { regular: 120 }
        ]
      )

      Pricing::Sku.create!(
        _id: 'hat',
        tax_code: '001',
        prices: [
          { regular: 50 }
        ]
      )

      # Create an inventory sku for each product variant
      all_skus = Catalog::Product.all.map(&:skus).flatten
      all_skus.each do |sku|
        Inventory::Sku.create!(
          _id: sku,
          policy: 'standard',
          available: '99'
        )
      end

      # Create an order with 2 items
      order = Order.create!

      ['large-shirt', '48-long-jacket'].each do |sku|
        product = Catalog::Product.find_by_sku(sku)
        order.add_item(product_id: product.id, sku: sku, quantity: 1)
      end

      # Create an order level discount
      order_discount = Pricing::Discount::OrderTotal.create!(
        name: 'Save $20 on an Order of $100 or More',
        amount_type: :flat,
        amount: 20,
        order_total: 100
      )

      # Create an item level discount
      jacket = Catalog::Product.find_by(name: 'Jacket')

      Pricing::Discount::Product.create!(
        name: '$10 Off Jacket',
        amount_type: :flat,
        amount: 10,
        product_ids: [jacket.id],
        compatible_discount_ids: [order_discount.id]
      )

      # Add a tax category with a rate
      tax_category = Tax::Category.create!(
        name: 'Sales Tax',
        code: '001'
      )

      Tax::Rate.create!(
        percentage: 0.10,
        country: 'US',
        region: 'PA',
        category: tax_category
      )

      # Add a shipping service
      Shipping::Service.create!(
        name: 'Standard',
        tax_code: '001',
        rates: [
          { price: 5 }
        ]
      )

      # Start a guest checkout
      checkout = Checkout.new(order)
      checkout.start_as(:guest)

      # Complete the addresses step of checkout
      addresses_step = Checkout::Steps::Addresses.new(checkout)

      address_fields = {
        first_name: 'Bobby',
        last_name: 'Clams',
        street: '22 S 3rd St',
        city: 'Philadelphia',
        region: 'PA',
        postal_code: '19106',
        country: 'US'
      }

      addresses_step.update(
        email: 'bclams@weblinc.com',
        shipping_address: address_fields,
        billing_address: address_fields
      )

      # Complete the payment step of checkout
      payment_step = Checkout::Steps::Payment.new(checkout)

      credit_card_fields = {
        number: '4111111111111111',
        cvv: '123',
        month: '12',
        year: '2017'
      }

      payment_step.update(payment: 'new_card', credit_card: credit_card_fields)

      # Complete the checkout (place the order)
      checkout.place_order

      # Mark all the items as 'shipped'
      fulfillment = Fulfillment.find(order.id)

      shipped_items = order.items.reduce([]) do |items, item|
        items << { 'id' => item.id.to_s, 'quantity' => item.quantity }
      end

      fulfillment.ship_items('abc123', shipped_items)

      # Flag some of the pricing skus "Big & Tall"
      big_and_tall_skus = [
        '2-extra-large-shirt',
        '3-extra-large-shirt',
        '48-long-jacket'
      ]

      Pricing::Sku
        .in(_id: big_and_tall_skus)
        .update_all(big_and_tall: true)

      # Add a pricing sku to represent a "Big & Tall" surcharge
      Pricing::Sku.create!(
        _id: 'big-and-tall-surcharge',
        tax_code: '001',
        prices: [
          { regular: 10 }
        ]
      )

      # Create a content page and add a 'text' block to its content
      page = Content::Page.create!(name: 'Big & Tall')

      placeholder_text = "<p>#{Workarea.config.placeholder_text}</p>" * 2

      Content.for(page).blocks.create!(
        type: 'text',
        data: { text: placeholder_text }
      )

      # Update all products to use the 'attributes' template
      Catalog::Product.all.each_by(100) do |product|
        product.template = 'attributes'
        product.save!
      end

      # Add a 'product line' block to the home page content
      big_and_tall_product_ids = Catalog::Product
                                  .in(name: ['Shirt', 'Jacket'])
                                  .pluck(:_id)

      big_and_tall_heading = '<h2>Largest Selection of Big & Tall</h2>'
      text = big_and_tall_heading + placeholder_text

      Content.for('home_page').blocks.create!(
        type: 'product_line',
        data: {
          text: text,
          products: big_and_tall_product_ids
        }
      )
    end
  end
end
