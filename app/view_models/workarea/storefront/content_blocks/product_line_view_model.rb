module Workarea
  module Storefront
    module ContentBlocks
      class ProductLineViewModel < ContentBlockViewModel
        def text
          data[:text]
        end

        def products
          return [] unless data['products'].present?

          @products ||= Catalog::Product
                          .find_ordered_for_display(data['products'])
                          .map { |product| ProductViewModel.wrap(product) }
        end
      end
    end
  end
end
