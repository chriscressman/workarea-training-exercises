module Workarea
  module Storefront
    module ProductTemplates
      class AttributeViewModel < ProductViewModel
        def product_attributes
          Hash[model.details.map { |k, v| [k, v.join(', ')] }]
        end
      end
    end
  end
end
