Workarea.configure do |config|
  # Basic site info
  config.site_name = 'Training App'
  config.host = 'training-app.dev'
  config.email_to = 'customerservice@training-app.dev'
  config.email_from = 'noreply@training-app.dev'

  config.seeds = Workarea::SwappableList.new('Workarea::TrainingSeeds')

  Workarea.config.pricing_calculators.insert_after(
    'Workarea::Pricing::Calculators::ItemCalculator',
    'Workarea::Pricing::Calculators::BigAndTallCalculator'
  )

  Workarea.append_stylesheets(
    'storefront.components',
    'workarea/storefront/components/badge',
    'workarea/storefront/components/product_line_content_block'
  )

  Workarea.append_partials(
    'storefront.product_details',
    'workarea/storefront/products/badge'
  )

  config.product_templates << :attributes

  Workarea::Content.define_block_types do
    block_type 'Product Line' do
      description 'Information about a product line and some example products from that line'
      field 'Text', :text, default: "<h2>About These Products</h2><p>#{Workarea.config.placeholder_text}</p>"
      field 'Products', :products, default: (lambda do
         result = Array.new(2) { Catalog::Product.sample.try(:id) }
         result.compact
      end)
    end
  end
end
